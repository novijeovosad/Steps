# dotnet mvc Sa_mee_rSa_ini blog
![](images/2023-05-06-08-31-25.png)

config  
![](images/2023-05-06-08-32-05.png)

model  
![](images/2023-05-06-08-40-49.png)

generate model\Domain BlogPost.cs  
![](images/2023-05-06-08-41-47.png)

BlogPost.cs model  
![](images/2023-05-06-08-42-32.png)
![](images/2023-05-06-08-42-49.png)
add Tag collection to BlogPost model  
![](images/2023-05-06-08-45-17.png)


generate Tag.cs model  
![](images/2023-05-06-08-43-40.png)


Tag.cs model  
![](images/2023-05-06-08-44-35.png)
add BlogPost collection to Tag model  
![](images/2023-05-06-08-46-21.png)

add nuget entity packages  
![](images/2023-05-06-08-47-53.png)
![](images/2023-05-06-08-48-09.png)

DbContext Class  
![](images/2023-05-06-08-48-40.png)
![](images/2023-05-06-08-48-55.png)

Generate Data\BloggieDbContext.cs  
![](images/2023-05-06-08-49-30.png)

BloggieDbContext.cs  
![](images/2023-05-06-08-51-01.png)

Add BlogPosts, DbSet  
![](images/2023-05-06-08-51-32.png)

database connection string  
![](images/2023-05-06-09-15-59.png)
![](images/2023-05-06-09-16-15.png)

appsettings.json  
![](images/2023-05-06-09-16-36.png)
![](images/2023-05-06-09-16-49.png)

Program.cs add UseSqlServer line 10  
![](images/2023-05-06-09-18-59.png)
![](images/2023-05-06-09-19-12.png)

migration command cli  
![](images/2023-05-06-09-20-02.png)
![](images/2023-05-06-09-20-16.png)

migrations  
![](images/2023-05-06-09-20-39.png)

updata-database cli command  
![](images/2023-05-06-09-21-49.png)
![](images/2023-05-06-09-22-03.png)

mvc  
![](images/2023-05-06-09-23-54.png)

HomeController  
![](images/2023-05-06-09-25-39.png)

Home index.cshtml  
![](images/2023-05-06-09-26-05.png)

generate AdminTagsController  
![](images/2023-05-06-09-29-25.png)

AdminTagsController  
![](images/2023-05-06-09-29-59.png)

Generate razor view Add.cshtml  
![](images/2023-05-06-09-30-55.png)

Add.cshtml  
![](images/2023-05-06-09-31-26.png)

Layout.cshtml  
![](images/2023-05-06-09-31-48.png)

rendered Add Tag  
![](images/2023-05-06-09-32-19.png)

Layout.cshtml  
![](images/2023-05-06-09-32-37.png)

model blogPost Tag  
![](images/2023-05-06-09-33-21.png)

Add.cshtml input forms  
![](images/2023-05-06-09-33-58.png)
![](images/2023-05-06-09-34-16.png)

AdminTagsController  
![](images/2023-05-06-09-35-27.png)

generate AddTagRequest  
![](images/2023-05-06-09-36-24.png)

AddTagRequest.cs  
![](images/2023-05-06-09-37-13.png)

Getters/Setters Name, DisplayName  
![](images/2023-05-06-09-37-44.png)

import model AddTagRequest in Add.cshtml  
![](images/2023-05-06-09-38-21.png)

asp-for='Name' input form  
![](images/2023-05-06-09-38-52.png)

AdminTagsController.cs  
![](images/2023-05-06-09-39-32.png)

constructor AdminTagsController  
![](images/2023-05-06-09-40-26.png)

Add() method AdminTagsController   
![](images/2023-05-06-09-41-28.png)

sql query  
![](images/2023-05-06-09-42-03.png)