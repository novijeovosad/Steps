
# #4 tuto amigos postgres
![](images/2023-04-10-15-57-29.png)

create table\
![](images/2023-04-10-15-58-10.png)

create table person \
![](images/2023-04-10-15-58-27.png)

create table person extra constraints \
![](images/2023-04-10-15-59-08.png)

insert records into table \
![](images/2023-04-10-16-00-13.png)

insert records into table \
![](images/2023-04-10-16-21-47.png)

mockaroo sql save\
![](images/2023-04-10-16-32-58.png)

\\i command\
![](images/2023-04-10-16-37-09.png)

foreign keys & joins\
![](images/2023-04-10-16-39-03.png)

person\
![](images/2023-04-10-16-39-24.png)

relation person car \
![](images/2023-04-10-16-39-50.png)

\\dt command\
![](images/2023-04-10-16-40-20.png)

mora se prvo car a onda person da budu povezani\
![](images/2023-04-10-16-41-44.png)

pazi kad radis update ili alter da koristis WHERE jer inace ces promijeniti cijelu tablicu\
![](images/2023-04-10-16-42-44.png)

inner join\
![](images/2023-04-10-16-43-27.png)

inner join command \
![](images/2023-04-10-16-44-21.png)

left join \
![](images/2023-04-10-16-44-43.png)

left join command \
![](images/2023-04-10-16-45-42.png)

export query with join to csv\
![](images/2023-04-10-16-47-33.png)

install extensions \
![](images/2023-04-10-16-49-20.png)

install extensions \
![](images/2023-04-10-16-49-47.png)

generate uuid \
![](images/2023-04-10-16-50-15.png)

kreiranje tablica sa uuid, ne treba redoslijed \
![](images/2023-04-10-16-51-22.png)
