# lara 03 codeWithDary property-listing
![](images/2023-04-26-03-23-49.png)

create auth cont   
![](images/2023-04-26-03-34-45.png)

AuthController - copied from gh repo  
![](images/2023-04-26-03-38-09.png)
![](images/2023-04-26-03-36-58.png)
![](images/2023-04-26-03-37-08.png)

Trait HttpResponses c/p gh  
![](images/2023-04-26-03-39-23.png)
![](images/2023-04-26-03-40-13.png)

generate Login request  
![](images/2023-04-26-03-41-05.png)

generate StoreUserRequest  
![](images/2023-04-26-03-41-32.png)

api.php add routes Login, Register, Logout  
![](images/2023-04-26-03-45-42.png)

postman register  
![](images/2023-04-26-03-46-48.png)

postman register body config  
![](images/2023-04-26-03-47-20.png)

postman register send (receive token)  
![](images/2023-04-26-03-48-24.png)

generate model Broker  
![](images/2023-04-26-03-56-15.png)

tables Broker  
![](images/2023-04-26-03-57-53.png)

model Broker  
![](images/2023-04-26-03-58-33.png)

generate model Property  
![](images/2023-04-26-04-03-39.png)

tables Property  
![](images/2023-04-26-04-04-26.png)
![](images/2023-04-26-04-04-48.png)

add enum to listing_type  
![](images/2023-04-26-04-06-13.png)

enum ListingType  
![](images/2023-04-26-04-06-51.png)

generate PropertyCharacteristic  
![](images/2023-04-26-04-08-29.png)

tables create_property_characteristics  
![](images/2023-04-26-04-09-30.png)
![](images/2023-04-26-04-09-58.png)
![](images/2023-04-26-04-14-25.png)
![](images/2023-04-26-04-14-45.png)

PropertyStatusEnum  
![](images/2023-04-26-04-13-16.png)

Property Model  
![](images/2023-04-26-04-15-13.png)

PropertyCharacteristic Model  
![](images/2023-04-26-04-15-43.png)

generate controller BrokersController  
![](images/2023-04-26-04-17-00.png)

stop => 28:00
![](images/2023-04-26-04-21-40.png)