# lara codeholic API v8
![](images/2023-05-02-23-34-11.png)
![](images/2023-05-02-23-35-20.png)

install lara  
![](images/2023-05-02-23-33-50.png)
![](images/2023-05-02-23-40-16.png)

baza  
![](images/2023-05-02-23-41-17.png)

generate model Album  
![](images/2023-05-02-23-43-16.png)

table create_albums  
![](images/2023-05-02-23-43-48.png)
![](images/2023-05-02-23-44-03.png)

generate model image_manipulations
![](images/2023-05-02-23-45-11.png)

table image_manipulations  
![](images/2023-05-02-23-48-40.png)

nullable table columns  
![](images/2023-05-05-07-26-23.png)

generate controller AlbumController  
![](images/2023-05-03-00-03-52.png)
![](images/2023-05-03-00-04-18.png)

route AlbumController  
![](images/2023-05-05-07-23-51.png)

model Album  
![](images/2023-05-05-07-25-24.png)

create_albums_table  
![](images/2023-05-05-07-27-17.png)

migration rollback change model  
![](images/2023-05-05-07-27-36.png)

migrate  
![](images/2023-05-05-07-28-00.png)

post api/album  
![](images/2023-05-05-07-28-33.png)

AlbumController store()  
![](images/2023-05-05-07-29-26.png)

StoreAlbumRequest authorize()  
![](images/2023-05-05-07-30-10.png)

AlbumController index()  
![](images/2023-05-05-07-31-04.png)

AlbumController show()  
![](images/2023-05-05-07-31-35.png)

AlbumController update()  
![](images/2023-05-05-07-32-00.png)

AlbumController destroy()   
![](images/2023-05-05-07-32-31.png)

put /api/album/1  
![](images/2023-05-05-07-33-18.png)

route v1/ middlevare  
![](images/2023-05-05-07-36-32.png)

test v1/  
![](images/2023-05-05-07-36-53.png)

generate AlbumResource  
![](images/2023-05-05-07-37-39.png)

example of generate AlbumController  
![](images/2023-05-05-07-38-46.png)

generate v1\\ImageManipulationController  
![](images/2023-05-05-07-40-11.png)

generate resource v1\\ImageManipulationResource  
![](images/2023-05-05-07-40-45.png)

AlbumResource toArray  
![](images/2023-05-05-07-41-59.png)

AlbumController  
![](images/2023-05-05-07-42-30.png)

AlbumController update()  
![](images/2023-05-05-07-43-28.png)

route image CRUD  
![](images/2023-05-05-07-44-35.png)