# dotnet api Sa_mee_rSa_ini

install  
![](images/2023-05-06-05-28-42.png)
![](images/2023-05-06-05-30-29.png)

test with swagger  ![](images/2023-05-06-05-32-10.png)

new class in Data folder ContactsApi.cs  
![](images/2023-05-06-05-35-05.png)

generate constructor - right click on class name  
![](images/2023-05-06-05-36-14.png)

property DBSet for crud  
![](images/2023-05-06-05-37-32.png)

add service to Program.cs  
![](images/2023-05-06-05-38-34.png)

add UseImMemoryDatabase as option  
![](images/2023-05-06-05-39-33.png)

generate AddContacts controller  
![](images/2023-05-06-05-40-33.png)

annotation for api on top  
![](images/2023-05-06-05-41-24.png)

change index() method to GetContacts()  
![](images/2023-05-06-05-42-33.png)

add constructor for dbContext  
![](images/2023-05-06-05-43-27.png)

wrap return with Ok() response  
![](images/2023-05-06-05-44-59.png)

AddContact method to ContactsController  
![](images/2023-05-06-06-03-53.png)

new Model AddContactRequest  
![](images/2023-05-06-06-04-24.png)

Contacts Model  
![](images/2023-05-06-06-04-50.png)

AddContactRequest model je isti kao Contacts ali bez ID, zato sto cemo ga sami napraviti  
![](images/2023-05-06-06-06-07.png)

u AddContact metodi pozoves model kao parametar  
![](images/2023-05-06-06-06-40.png)

stavis sve u await async i vratis ok response  
![](images/2023-05-06-06-07-14.png)

async-await i GetContacts()  
![](images/2023-05-06-07-11-47.png)

test swagger post  
![](images/2023-05-06-07-13-57.png)
![](images/2023-05-06-07-14-10.png)

update method  
![](images/2023-05-06-07-16-39.png)

model za UpdateContact isti kao AddContact, ali samo u ovom slucaju, nekad nije isto  
![](images/2023-05-06-07-18-39.png)

dodaj model updateCotnact kao parametar  
![](images/2023-05-06-07-18-58.png)

async-await i nadopuni sa request poljima  
![](images/2023-05-06-07-19-55.png)

potvrdi sa 200Ok return response  
![](images/2023-05-06-07-20-53.png)

test za put swagger, mora biti tocan id  
![](images/2023-05-06-07-21-29.png)

get single contact  
![](images/2023-05-06-08-06-57.png)

return ok  
![](images/2023-05-06-08-07-36.png)

delete  
![](images/2023-05-06-08-08-08.png)
![](images/2023-05-06-08-08-23.png)

mozes staviti ok response ako je uspjelo brisanje  
![](images/2023-05-06-08-09-04.png)

nuget add entity  
![](images/2023-05-06-08-13-47.png)

entity classes  
![](images/2023-05-06-08-14-26.png)

mssql connection  
![](images/2023-05-06-08-15-14.png)

umjesto inmemory db stavis mssql  
![](images/2023-05-06-08-15-59.png)

entity database migrate cli command  
![](images/2023-05-06-08-17-08.png)
![](images/2023-05-06-08-17-26.png)